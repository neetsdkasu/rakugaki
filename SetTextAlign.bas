Attribute VB_Name = "Win32API_STA"
Option Explicit

Public Declare Function SetTextAlign Lib "gdi32" ( _
    ByVal hDC As Long, _
    ByVal wFlags As Long) As Long
Public Const TA_UPDATECP = 1
Public Const TA_TOP = 0
Public Const TA_LEFT = 0
Public Const TA_NOUPDATECP = 0
Public Const TA_BASELINE = 24
Public Const TA_BOTTOM = 8
Public Const TA_CENTER = 6
Public Const TA_RIGHT = 2
Public Const TA_DEFAULT = TA_TOP Or TA_LEFT Or TA_NOUPDATECP


