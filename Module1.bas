Attribute VB_Name = "Module1"
Option Explicit

Public F1W As Long
Public F1H As Long
Public dFill As Integer
Public dMode As Integer
Public dModeBefore As Integer
Public dModeStg As String
Public dModeStg2 As String
Public dWidth As Integer
Public dPoint As Integer
Public dColor As Long
Public dcRed As Integer
Public dcBlue As Integer
Public dcGreen As Integer
Public dText As String

Public Sub Chng_dFill()
    dFill = 1 - dFill
    If dFill = 1 Then
        Form3.Command4.BackColor = QBColor(7)
        Form2.PM_dFill.Checked = False
    Else
        Form3.Command4.BackColor = vbRed
        Form2.PM_dFill.Checked = True
    End If
    Form1.FillStyle = dFill
    Form1.Shape1.FillStyle = dFill
    Form1.Shape2.FillStyle = dFill
    Form2.Picture4.FillStyle = dFill
    Form2.Picture5.FillStyle = dFill
End Sub

Public Sub Chng_dcRed()
    Form3.Label1.Caption = "R " + Format$(dcRed, "000")
    Form2.PM_RedValue.Caption = "Now (" + Format$(dcRed, "000") + ")"
    Call Chng_dColor
End Sub

Public Sub Chng_dcGreen()
    Form3.Label2.Caption = "G " + Format$(dcGreen, "000")
    Form2.PM_GreenValue.Caption = "Now (" + Format$(dcGreen, "000") + ")"
    Call Chng_dColor
End Sub

Public Sub Chng_dcBlue()
    Form3.Label3.Caption = "B " + Format$(dcBlue, "000")
    Form2.PM_BlueValue.Caption = "Now (" + Format$(dcBlue, "000") + ")"
    Call Chng_dColor
End Sub

Public Sub Chng_dColor()
    dColor = RGB(dcRed, dcGreen, dcBlue)
    Form3.Picture1.BackColor = dColor
    Form3.Refresh
    Form2.Picture3.BackColor = dColor
End Sub

Public Sub Chng_Color(cc As Long)
    Dim r As Integer
    Dim g As Integer
    Dim b As Integer
    Dim c As Long
    c = cc
    b = CInt(c \ &H10000)
    c = c Mod &H10000
    g = CInt(c \ &H100)
    c = c Mod &H100
    r = CInt(c)
    Form3.HScroll1.Value = r
    Form3.HScroll2.Value = g
    Form3.HScroll3.Value = b
End Sub

Public Sub Chng_dWidth()
    Form2.PM_dWidthValue.Caption = "Now (" + Format$(dWidth, "00") + ")"
    Form3.Label4.Caption = "Pen Width " + Format$(dWidth, "00")
    Form3.Refresh
    Call F2Caption
End Sub

Public Function SmallL(a As Single, b As Single) As Single
    If a > b Then
        SmallL = b
    Else
        SmallL = a
    End If
End Function

Public Function GapL(a As Single, b As Single) As Single
    If a > b Then
        GapL = a - b
    ElseIf b > a Then
        GapL = b - a
    Else
        GapL = 1
    End If
End Function

Public Function LenX(s As String) As Integer
    Dim a As Integer
    Dim i As Integer
    Dim l As Integer
    Dim m As Integer
    l = 0
    m = CInt(Len(s))
    For i = 1 To m
        a = Asc(Mid$(s, i, 1))
        If &H0 <= a And a <= &HFF Then
            l = l + 1
        Else
            l = l + 2
        End If
    Next i
    LenX = l
End Function

Public Sub F2Caption()
    Dim s As String
    Dim t As String
    s = "": t = ""
    If dPoint = 1 Then s = "*"
    If dMode = 6 Then t = "[" + dText + "]"
    Form2.Caption = dModeStg + "�F" + s + dModeStg2 + "(" + Format$(dWidth) + ")" + t
End Sub

Public Sub dPoint_Start(X As Single, Y As Single)
    Form1.Label4.Move X + 20 + (X > F1W - 120) * 75, Y - 20 - (Y < 100) * 35
    Form1.Label4.Caption = Format$(X, "###0") + "x" + Format$(Y, "###0")
    Form1.Line2.X1 = X
    Form1.Line2.Y1 = Y
    Form1.Line2.X2 = Form1.Label4.Left - (X > F1W - 120) * (Form1.Label4.Width - 1)
    Form1.Line2.Y2 = Form1.Label4.Top - (Y >= 100) * (Form1.Label4.Height - 1)
    Form1.Label4.Visible = True
    Form1.Line2.Visible = True
End Sub

Public Sub dPoint_Size(X As Single, Y As Single, s As String)
    Form1.Label3.Visible = True
    Form1.Label3.Caption = s
    Form1.Label3.Move X + 30 + (X > F1W - 120) * 100, Y + 45 + (Y > F1H - 100) * 70
End Sub


Public Sub dText_Edit()
    Dim s As String
    s = dText
    dText = InputBox("Input Printing Text", "Text", s)
    If Len(dText) > 20 Then s = Left$(dText, 17) + "..." Else s = dText
    Form2.PM_TShow.Caption = "Text::" + s
    Form3.Label5.Caption = "Text::" + dText
    Call F2Caption
End Sub

Public Sub Drawing_Spray(X As Single, Y As Single)
    Dim i As Integer
    Dim r As Single
    Dim rd As Double
    Form1.DrawWidth = 1
    Form2.Picture4.DrawWidth = 1
    Form2.Picture5.DrawWidth = 1
    For i = 0 To dWidth * 3
        r = Rnd() * CSng(dWidth * 2 + 10)
        rd = CDbl(Rnd() * CSng(6.28))
        Form1.PSet (X + r * CSng(Cos(rd)), Y + r * CSng(Sin(rd))), dColor
        Form2.Picture4.PSet (X + r * CSng(Cos(rd)), Y + r * CSng(Sin(rd))), vbBlack
        Form2.Picture5.PSet (X + r * CSng(Cos(rd)), Y + r * CSng(Sin(rd))), dColor
    Next i
End Sub

Public Sub Make_Picture()
    Form2.Picture1.Picture = Form2.Picture2.Image
    BitBlt Form2.Picture1.hDC, 0, 0, F1W, F1H, Form2.Picture4.hDC, 0, 0, SRCAND
    BitBlt Form2.Picture1.hDC, 0, 0, F1W, F1H, Form2.Picture5.hDC, 0, 0, SRCPAINT
End Sub

Public Sub Load_Desktop()
    Dim hDskTp As Long
    Dim hDskTpDC As Long
    hDskTp = GetDesktopWindow()
    hDskTpDC = GetWindowDC(hDskTp)
    BitBlt Form2.Picture2.hDC, 0, 0, F1W, F1H, hDskTpDC, 0, 0, SRCCOPY
    Call ReleaseDC(hDskTp, hDskTpDC)
End Sub
