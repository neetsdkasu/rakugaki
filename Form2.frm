VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "Form2"
   ClientHeight    =   375
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2295
   ControlBox      =   0   'False
   Icon            =   "Form2.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   375
   ScaleWidth      =   2295
   StartUpPosition =   3  'Windows の既定値
   Begin VB.PictureBox Picture7 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   1800
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   12
      Top             =   2760
      Width           =   1455
   End
   Begin VB.PictureBox Picture6 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   120
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   11
      Top             =   2760
      Width           =   1455
   End
   Begin VB.CommandButton Command6 
      Caption         =   "終"
      Height          =   375
      Left            =   1920
      TabIndex        =   10
      Top             =   0
      Width           =   375
   End
   Begin VB.Timer Timer2 
      Left            =   2880
      Top             =   0
   End
   Begin VB.PictureBox Picture5 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   1800
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   9
      Top             =   1680
      Width           =   1455
   End
   Begin VB.PictureBox Picture4 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   120
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   8
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Timer Timer1 
      Left            =   2400
      Top             =   0
   End
   Begin VB.PictureBox Picture3 
      AutoRedraw      =   -1  'True
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   75
      TabIndex        =   7
      Top             =   0
      Width           =   135
   End
   Begin VB.CommandButton Command5 
      Caption         =   "戻"
      Height          =   375
      Left            =   1560
      TabIndex        =   6
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command4 
      Caption         =   "設"
      Height          =   375
      Left            =   1200
      TabIndex        =   5
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command3 
      Caption         =   "消"
      Height          =   375
      Left            =   840
      TabIndex        =   4
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command2 
      Caption         =   "停"
      Height          =   375
      Left            =   480
      TabIndex        =   3
      Top             =   0
      Width           =   375
   End
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   1800
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   2
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "描"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   375
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   855
      Left            =   120
      ScaleHeight     =   57
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   97
      TabIndex        =   0
      Top             =   600
      Width           =   1455
   End
   Begin VB.Menu PMenu 
      Caption         =   "ポップアップメニュー"
      Enabled         =   0   'False
      Visible         =   0   'False
      Begin VB.Menu PM_DrawStyle 
         Caption         =   "DrawStyle"
         Begin VB.Menu PM_dMode 
            Caption         =   "Pen"
            Checked         =   -1  'True
            Index           =   0
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Line"
            Index           =   1
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Spray"
            Index           =   2
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Round"
            Index           =   3
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Rect"
            Index           =   4
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Spoit"
            Index           =   5
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Text"
            Index           =   6
         End
         Begin VB.Menu PM_dMode 
            Caption         =   "Erase"
            Index           =   7
         End
         Begin VB.Menu PM_Bar05 
            Caption         =   "-"
         End
         Begin VB.Menu PM_dFill 
            Caption         =   "Fill"
         End
      End
      Begin VB.Menu PM_Bar01 
         Caption         =   "-"
      End
      Begin VB.Menu PM_dWidth 
         Caption         =   "Pen Width"
         Begin VB.Menu PM_dWidthValue 
            Caption         =   "Now (00)"
            Enabled         =   0   'False
         End
         Begin VB.Menu PM_dWidthMax 
            Caption         =   "Max"
         End
         Begin VB.Menu PM_dWidthHalf 
            Caption         =   "Half"
         End
         Begin VB.Menu PM_dWidthMin 
            Caption         =   "Min"
         End
         Begin VB.Menu PM_dWidthUp5 
            Caption         =   "＋５"
         End
         Begin VB.Menu PM_dWidthUp3 
            Caption         =   "＋３"
         End
         Begin VB.Menu PM_dWidthUp1 
            Caption         =   "＋１"
         End
         Begin VB.Menu PM_dWidthDown1 
            Caption         =   "−１"
         End
         Begin VB.Menu PM_dWidthDown3 
            Caption         =   "−３"
         End
         Begin VB.Menu PM_dWidthDown5 
            Caption         =   "−５"
         End
      End
      Begin VB.Menu PM_Bar02 
         Caption         =   "-"
      End
      Begin VB.Menu PM_DefColor 
         Caption         =   "Default Color"
         Begin VB.Menu PM_DCBlack 
            Caption         =   "Black"
         End
         Begin VB.Menu PM_DCBlue 
            Caption         =   "Blue"
         End
         Begin VB.Menu PM_DCCyan 
            Caption         =   "Cyan"
         End
         Begin VB.Menu PM_DCGreen 
            Caption         =   "Green"
         End
         Begin VB.Menu PM_DCMagenta 
            Caption         =   "Magenta"
         End
         Begin VB.Menu PM_DCRed 
            Caption         =   "Red"
         End
         Begin VB.Menu PM_DCWhite 
            Caption         =   "White"
         End
         Begin VB.Menu PM_DCYellow 
            Caption         =   "Yellow"
         End
      End
      Begin VB.Menu PM_Red 
         Caption         =   "RGB (Red)"
         Begin VB.Menu PM_RedValue 
            Caption         =   "Now (000)"
            Enabled         =   0   'False
         End
         Begin VB.Menu PM_RedMax 
            Caption         =   "Max"
         End
         Begin VB.Menu PM_RedHalf 
            Caption         =   "Half"
         End
         Begin VB.Menu PM_RedMin 
            Caption         =   "Min"
         End
         Begin VB.Menu PM_RedUp50 
            Caption         =   "＋５０"
         End
         Begin VB.Menu PM_RedUp25 
            Caption         =   "＋２５"
         End
         Begin VB.Menu PM_RedUp05 
            Caption         =   "＋５"
         End
         Begin VB.Menu PM_RedUp01 
            Caption         =   "＋１"
         End
         Begin VB.Menu PM_RedDown01 
            Caption         =   "−１"
         End
         Begin VB.Menu PM_RedDown05 
            Caption         =   "−５"
         End
         Begin VB.Menu PM_RedDown25 
            Caption         =   "−２５"
         End
         Begin VB.Menu PM_RedDown50 
            Caption         =   "−５０"
         End
      End
      Begin VB.Menu PM_Green 
         Caption         =   "RGB (Green)"
         Begin VB.Menu PM_GreenValue 
            Caption         =   "Now (000)"
            Enabled         =   0   'False
         End
         Begin VB.Menu PM_GreenMax 
            Caption         =   "Max"
         End
         Begin VB.Menu PM_GreenHalf 
            Caption         =   "Half"
         End
         Begin VB.Menu PM_GreenMin 
            Caption         =   "Min"
         End
         Begin VB.Menu PM_GreenUp50 
            Caption         =   "＋５０"
         End
         Begin VB.Menu PM_GreenUp25 
            Caption         =   "＋２５"
         End
         Begin VB.Menu PM_GreenUp05 
            Caption         =   "＋５"
         End
         Begin VB.Menu PM_GreenUp01 
            Caption         =   "＋１"
         End
         Begin VB.Menu PM_GreenDown01 
            Caption         =   "−１"
         End
         Begin VB.Menu PM_GreenDown05 
            Caption         =   "−５"
         End
         Begin VB.Menu PM_GreenDown25 
            Caption         =   "−２５"
         End
         Begin VB.Menu PM_GreenDown50 
            Caption         =   "−５０"
         End
      End
      Begin VB.Menu PM_Blue 
         Caption         =   "RGB (Blue)"
         Begin VB.Menu PM_BlueValue 
            Caption         =   "Now (000)"
            Enabled         =   0   'False
         End
         Begin VB.Menu PM_BlueMax 
            Caption         =   "Max"
         End
         Begin VB.Menu PM_BlueHalf 
            Caption         =   "Half"
         End
         Begin VB.Menu PM_BlueMin 
            Caption         =   "Min"
         End
         Begin VB.Menu PM_BlueUp50 
            Caption         =   "＋５０"
         End
         Begin VB.Menu PM_BlueUp25 
            Caption         =   "＋２５"
         End
         Begin VB.Menu PM_BlueUp05 
            Caption         =   "＋５"
         End
         Begin VB.Menu PM_BlueUp01 
            Caption         =   "＋１"
         End
         Begin VB.Menu PM_BlueDown01 
            Caption         =   "−１"
         End
         Begin VB.Menu PM_BlueDown05 
            Caption         =   "−５"
         End
         Begin VB.Menu PM_BlueDown25 
            Caption         =   "−２５"
         End
         Begin VB.Menu PM_BlueDown50 
            Caption         =   "−５０"
         End
      End
      Begin VB.Menu PM_Bar03 
         Caption         =   "-"
      End
      Begin VB.Menu PM_Text 
         Caption         =   "DrawText"
         Begin VB.Menu PM_TEdit 
            Caption         =   "Edit"
         End
         Begin VB.Menu PM_TShow 
            Caption         =   "Text::"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu PM_Bar04 
         Caption         =   "-"
      End
      Begin VB.Menu PM_Cancel 
         Caption         =   "Cancel"
      End
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Form2.Command1.Enabled = False
    Form2.Command2.Enabled = True
    Form2.Command3.Enabled = True
    Form2.Command4.Enabled = True
    Form2.Visible = False
    Form2.Timer1.Interval = 2
End Sub

Private Sub Command2_Click()
    Form2.Command1.Enabled = True
    Form2.Command2.Enabled = False
    Form2.Command3.Enabled = False
    Form2.Command4.Enabled = False
    Form2.Command5.Enabled = False
    Form1.Visible = False
    Form3.Visible = False
    dModeStg = Form2.Command2.Caption
    Call F2Caption
End Sub

Private Sub Command3_Click()
    Form2.Picture1.Picture = Picture2.Image
    Form2.Picture4.Cls
    Form2.Picture4.BackColor = vbWhite
    Form2.Picture5.Cls
    Form2.Picture5.BackColor = vbBlack
    Form2.Picture6.Cls
    Form2.Picture7.Cls
    BitBlt Form1.hDC, 0, 0, F1W, F1H, Form2.Picture1.hDC, 0, 0, SRCCOPY
    Form1.Refresh
End Sub

Private Sub Command4_Click()
    Form3.Visible = True
End Sub

Private Sub Command5_Click()
    Form2.Picture4.Picture = Form2.Picture6.Image
    Form2.Picture5.Picture = Form2.Picture7.Image
    Call Make_Picture
    Form1.Picture = Form2.Picture1.Image
    Form2.Command5.Enabled = False
End Sub

Private Sub Command6_Click()
    Form1.Visible = False
    Form2.Visible = False
    Form3.Visible = False
    Form2.Timer2.Interval = 2
End Sub

Private Sub Form_Load()
    Dim hDskTp As Long
    Dim hDskTpDC As Long
    Dim F1Wt As Single
    Dim F1Ht As Single
    F1Wt = Form1.Width
    F1Ht = Form1.Height
    Form2.Picture1.Width = F1Wt
    Form2.Picture1.Height = F1Ht
    Form2.Picture2.Width = F1Wt
    Form2.Picture2.Height = F1Ht
    Form2.Picture4.Width = F1Wt
    Form2.Picture4.Height = F1Ht
    Form2.Picture5.Width = F1Wt
    Form2.Picture5.Height = F1Ht
    Form2.Picture6.Width = F1Wt
    Form2.Picture6.Height = F1Ht
    Form2.Picture7.Width = F1Wt
    Form2.Picture7.Height = F1Ht
    hDskTp = GetDesktopWindow()
    hDskTpDC = GetWindowDC(hDskTp)
    BitBlt Form2.Picture2.hDC, 0, 0, F1W, F1H, hDskTpDC, 0, 0, SRCCOPY
    Call ReleaseDC(hDskTp, hDskTpDC)
    Form2.Picture1.Picture = Form2.Picture2.Image
    Form2.Picture4.BackColor = vbWhite
    Form2.Picture5.BackColor = vbBlack
    Form2.Picture1.Visible = False
    Form2.Picture2.Visible = False
    Form2.Picture4.Visible = False
    Form2.Picture5.Visible = False
    Form2.Picture6.Visible = False
    Form2.Picture7.Visible = False
    Form2.Command2.Enabled = False
    Form2.Command3.Enabled = False
    Form2.Command4.Enabled = False
    Form2.Command5.Enabled = False
    Form2.Picture3.BackColor = dColor
    Call F2Caption
    Form2.Top = 0#
    Form2.Left = F1Wt - Form2.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Picture3_Click()
    dPoint = 1 - dPoint
    If dPoint = 0 Then
        Form1.Label2.Visible = False
        Form1.Label3.Visible = False
        Form1.Label4.Visible = False
    Else
        Form1.Label2.Visible = True
        Form1.Label2.Caption = ""
    End If
    Call F2Caption
End Sub

Private Sub PM_BlueDown01_Click()
    If dcBlue < 1 Then Exit Sub
    dcBlue = dcBlue - 1
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueDown05_Click()
    If dcBlue < 5 Then Exit Sub
    dcBlue = dcBlue - 5
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueDown25_Click()
    If dcBlue < 25 Then Exit Sub
    dcBlue = dcBlue - 25
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueDown50_Click()
    If dcBlue < 50 Then Exit Sub
    dcBlue = dcBlue - 50
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueHalf_Click()
    dcBlue = 128
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueMax_Click()
    dcBlue = 255
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueMin_Click()
    dcBlue = 0
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueUp01_Click()
    If dcBlue > 254 Then Exit Sub
    dcBlue = dcBlue + 1
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueUp05_Click()
    If dcBlue > 250 Then Exit Sub
    dcBlue = dcBlue + 5
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueUp25_Click()
    If dcBlue > 230 Then Exit Sub
    dcBlue = dcBlue + 25
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_BlueUp50_Click()
    If dcBlue > 205 Then Exit Sub
    dcBlue = dcBlue + 50
    Form3.HScroll3.Value = dcBlue
    Call Chng_dcBlue
End Sub

Private Sub PM_DCBlack_Click()
    Call Chng_Color(vbBlack)
End Sub

Private Sub PM_DCBlue_Click()
    Call Chng_Color(vbBlue)
End Sub

Private Sub PM_DCCyan_Click()
    Call Chng_Color(vbCyan)
End Sub

Private Sub PM_DCGreen_Click()
    Call Chng_Color(vbGreen)
End Sub

Private Sub PM_DCMagenta_Click()
    Call Chng_Color(vbMagenta)
End Sub

Private Sub PM_DCRed_Click()
    Call Chng_Color(vbRed)
End Sub

Private Sub PM_DCWhite_Click()
    Call Chng_Color(vbWhite)
End Sub

Private Sub PM_DCYellow_Click()
    Call Chng_Color(vbYellow)
End Sub

Private Sub PM_dFill_Click()
    Call Chng_dFill
End Sub

Private Sub PM_dMode_Click(Index As Integer)
    Dim i As Integer
    If Index = 5 Then
        If dMode <> 5 Then
            dModeBefore = dMode
        End If
    End If
    dMode = Index
    For i = 0 To 7
        Form2.PM_dMode(i).Checked = False
        Form3.Command2(i).BackColor = QBColor(7)
    Next i
    Form3.Command2(dMode).BackColor = vbYellow
    Form2.PM_dMode(dMode).Checked = True
    dModeStg2 = Form3.Command2(dMode).Caption
    Call F2Caption
End Sub

Private Sub PM_dWidthDown1_Click()
    If dWidth = 1 Then Exit Sub
    dWidth = dWidth - 1
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthDown3_Click()
    If dWidth < 4 Then Exit Sub
    dWidth = dWidth - 3
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthDown5_Click()
    If dWidth < 6 Then Exit Sub
    dWidth = dWidth - 5
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthHalf_Click()
    dWidth = 10
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthMax_Click()
    dWidth = 20
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthMin_Click()
    dWidth = 1
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthUp1_Click()
    If dWidth = 20 Then Exit Sub
    dWidth = dWidth + 1
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthUp3_Click()
    If dWidth > 17 Then Exit Sub
    dWidth = dWidth + 3
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_dWidthUp5_Click()
    If dWidth > 15 Then Exit Sub
    dWidth = dWidth + 5
    Form3.HScroll4.Value = dWidth
    Call Chng_dWidth
End Sub

Private Sub PM_GreenDown01_Click()
    If dcGreen < 1 Then Exit Sub
    dcGreen = dcGreen - 1
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenDown05_Click()
    If dcGreen < 5 Then Exit Sub
    dcGreen = dcGreen - 5
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenDown25_Click()
    If dcGreen < 25 Then Exit Sub
    dcGreen = dcGreen - 25
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenDown50_Click()
    If dcGreen < 50 Then Exit Sub
    dcGreen = dcGreen - 50
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenHalf_Click()
    dcGreen = 128
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenMax_Click()
    dcGreen = 255
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenMin_Click()
    dcGreen = 0
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenUp01_Click()
    If dcGreen > 254 Then Exit Sub
    dcGreen = dcGreen + 1
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenUp05_Click()
    If dcGreen > 250 Then Exit Sub
    dcGreen = dcGreen + 5
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenUp25_Click()
    If dcGreen > 230 Then Exit Sub
    dcGreen = dcGreen + 25
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_GreenUp50_Click()
    If dcGreen > 205 Then Exit Sub
    dcGreen = dcGreen + 50
    Form3.HScroll2.Value = dcGreen
    Call Chng_dcGreen
End Sub

Private Sub PM_RedDown01_Click()
    If dcRed < 1 Then Exit Sub
    dcRed = dcRed - 1
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedDown05_Click()
    If dcRed < 5 Then Exit Sub
    dcRed = dcRed - 5
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedDown25_Click()
   If dcRed < 25 Then Exit Sub
    dcRed = dcRed - 25
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedDown50_Click()
    If dcRed < 50 Then Exit Sub
    dcRed = dcRed - 50
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedHalf_Click()
    dcRed = 128
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedMax_Click()
    dcRed = 255
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedMin_Click()
    dcRed = 0
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedUp01_Click()
    If dcRed > 254 Then Exit Sub
    dcRed = dcRed + 1
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedUp05_Click()
    If dcRed > 250 Then Exit Sub
    dcRed = dcRed + 25
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedUp25_Click()
    If dcRed > 230 Then Exit Sub
    dcRed = dcRed + 25
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_RedUp50_Click()
    If dcRed > 205 Then Exit Sub
    dcRed = dcRed + 50
    Form3.HScroll1.Value = dcRed
    Call Chng_dcRed
End Sub

Private Sub PM_TEdit_Click()
    Call dText_Edit
End Sub

Private Sub Timer1_Timer()
    Timer1.Interval = 0
    Call Load_Desktop
    Call Make_Picture
    Form1.Picture = Form2.Picture1.Image
    Form1.Visible = True
    Form2.Visible = True
    dModeStg = Form2.Command1.Caption
    Call F2Caption
End Sub

Private Sub Timer2_Timer()
    Timer2.Interval = 0
    Call Load_Desktop
    Call Make_Picture
    Dim hnd As Long
    hnd = CreateDC("DISPLAY", vbNullString, vbNullString, 0&)
    BitBlt hnd, 0, 0, F1W, F1H, Form2.Picture1.hDC, 0, 0, SRCCOPY
    Call DeleteDC(hnd)
    Unload Form2
End Sub
