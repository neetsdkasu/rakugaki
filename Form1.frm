VERSION 5.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'なし
   Caption         =   "Form1"
   ClientHeight    =   2610
   ClientLeft      =   105
   ClientTop       =   105
   ClientWidth     =   3480
   ControlBox      =   0   'False
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   174
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   232
   StartUpPosition =   3  'Windows の既定値
   WindowState     =   2  '最大化
   Begin VB.Label Label3 
      Appearance      =   0  'ﾌﾗｯﾄ
      AutoSize        =   -1  'True
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   1  '実線
      Caption         =   "Label3"
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   1560
      TabIndex        =   3
      Top             =   2280
      Width           =   525
   End
   Begin VB.Label Label2 
      Appearance      =   0  'ﾌﾗｯﾄ
      AutoSize        =   -1  'True
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  '実線
      Caption         =   "Label2"
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   1560
      TabIndex        =   2
      Top             =   1920
      Width           =   525
   End
   Begin VB.Shape Shape4 
      BorderStyle     =   3  '点線
      DrawMode        =   10  'Mask Pen
      Height          =   615
      Left            =   2640
      Top             =   1320
      Width           =   735
   End
   Begin VB.Line Line2 
      DrawMode        =   10  'Mask Pen
      X1              =   168
      X2              =   168
      Y1              =   88
      Y2              =   128
   End
   Begin VB.Label Label4 
      Appearance      =   0  'ﾌﾗｯﾄ
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFC0&
      BorderStyle     =   1  '実線
      Caption         =   "Label4"
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   2640
      TabIndex        =   1
      Top             =   2160
      Width           =   525
   End
   Begin VB.Shape Shape3 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  '塗りつぶし
      Height          =   615
      Left            =   2520
      Shape           =   1  '正方形
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  '透明
      Caption         =   "Label1"
      Height          =   180
      Left            =   720
      TabIndex        =   0
      Top             =   1920
      Width           =   495
   End
   Begin VB.Shape Shape2 
      Height          =   1215
      Left            =   1560
      Top             =   480
      Width           =   735
   End
   Begin VB.Shape Shape1 
      Height          =   1215
      Left            =   600
      Shape           =   2  '楕円
      Top             =   480
      Width           =   735
   End
   Begin VB.Line Line1 
      X1              =   24
      X2              =   24
      Y1              =   32
      Y2              =   112
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim MouseFlag As Integer
Dim MouseX As Single
Dim MouseY As Single

Private Sub Form_Load()
    dFill = 1
    dMode = 0
    dModeBefore = 0
    dModeStg = "停"
    dModeStg2 = "Pen"
    dWidth = 3
    dPoint = 0
    dcRed = 0
    dcBlue = 0
    dcGreen = 0
    dColor = vbBlack
    dText = ""
    MouseFlag = 0
    Form1.Label1.Visible = False
    Form1.Line1.Visible = False
    Form1.Line2.Visible = False
    Form1.Shape1.Visible = False
    Form1.Shape2.Visible = False
    Form1.Shape3.Visible = False
    Form1.Shape4.Visible = False
    Form1.Label2.Visible = False
    Form1.Label3.Visible = False
    Form1.Label4.Visible = False
    Form1.Show
    F1W = CLng(Form1.ScaleWidth)
    F1H = CLng(Form1.ScaleHeight)
    Form1.Visible = False
    Form2.Show 0, Form1
    Form3.Show 0, Form1
    Form3.Visible = False
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If MouseFlag = 0 Then
        If Button = vbRightButton Then Exit Sub
        MouseFlag = 1
        Form2.Command5.Enabled = True
        Form2.Picture6.Picture = Form2.Picture4.Image
        Form2.Picture7.Picture = Form2.Picture5.Image
        Select Case dMode
        Case 0
            MouseX = X
            MouseY = Y
            Form1.DrawWidth = dWidth
            Form2.Picture4.DrawWidth = dWidth
            Form2.Picture5.DrawWidth = dWidth
            Form1.PSet (X, Y), dColor
            Form2.Picture4.PSet (X, Y), vbBlack
            Form2.Picture5.PSet (X, Y), dColor
        Case 1
            MouseX = X
            MouseY = Y
            Form1.Line1.X1 = X
            Form1.Line1.Y1 = Y
            Form1.Line1.X2 = X
            Form1.Line1.Y2 = Y
            Form1.Line1.BorderWidth = dWidth
            Form1.Line1.BorderColor = dColor
            Form1.Line1.Visible = True
            If dPoint = 1 Then
                Call dPoint_Start(X, Y)
            End If
        Case 2
            Call Drawing_Spray(X, Y)
        Case 3
            MouseX = X
            MouseY = Y
            Form1.Shape1.Move X, Y, 1, 1
            Form1.Shape4.Move X, Y, 1, 1
            Form1.Shape1.BorderWidth = dWidth
            Form1.Shape1.BorderColor = dColor
            Form1.Shape1.FillColor = dColor
            Form1.Shape1.Visible = True
            Form1.Shape4.Visible = True
            If dPoint = 1 Then
                Call dPoint_Start(X, Y)
            End If
        Case 4
            MouseX = X
            MouseY = Y
            Form1.Shape2.Move X, Y, 1, 1
            Form1.Shape4.Move X, Y, 1, 1
            Form1.Shape2.BorderWidth = dWidth
            Form1.Shape2.BorderColor = dColor
            Form1.Shape2.FillColor = dColor
            Form1.Shape2.Visible = True
            Form1.Shape4.Visible = True
            If dPoint = 1 Then
                Call dPoint_Start(X, Y)
            End If
        Case 6
            Form1.Label1.Caption = dText
            Form1.Label1.ForeColor = dColor
            Form1.Label1.FontSize = 10 + dWidth * 3
            Form1.Label1.Move X, Y
            Form1.Label1.Visible = True
        Case 7
            Dim hX As Single
            Dim hY As Single
            Dim w As Single
            Dim h As Single
            Form1.Shape3.Width = CSng(dWidth * 4 + 5)
            Form1.Shape3.Height = CSng(dWidth * 4 + 5)
            w = Form1.Shape3.Width
            h = Form1.Shape3.Height
            hX = X - w * 0.5
            hY = Y - h * 0.5
            BitBlt Form1.hDC, CLng(hX), CLng(hY), CLng(w), CLng(h), Form2.Picture2.hDC, CLng(hX), CLng(hY), SRCCOPY
            Form2.Picture4.Line (hX, hY)-(hX + w, hY + h), vbWhite, BF
            Form2.Picture5.Line (hX, hY)-(hX + w, hY + h), vbBlack, BF
            Form1.Shape3.Move hX, hY
            Form1.Shape3.Visible = True
        Case Else
        End Select
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If dPoint = 1 Then
        Form1.Label2.Caption = Format$(X, "###0") + "x" + Format$(Y, "###0")
        Form1.Label2.Move X + 30 + (X > F1W - 120) * 100, Y + 30 + (Y > F1H - 100) * 70
    End If
    If MouseFlag = 0 Then
        If Button = vbRightButton Then Exit Sub
    ElseIf MouseFlag = 1 Then
        Dim sX As Single
        Dim sY As Single
        Dim tX As Single
        Dim tY As Single
        Select Case dMode
        Case 0
            Form1.DrawWidth = dWidth
            Form2.Picture4.DrawWidth = dWidth
            Form2.Picture5.DrawWidth = dWidth
            Form1.Line (X, Y)-(MouseX, MouseY), dColor
            Form2.Picture4.Line (X, Y)-(MouseX, MouseY), vbBlack
            Form2.Picture5.Line (X, Y)-(MouseX, MouseY), dColor
            MouseX = X
            MouseY = Y
        Case 1
            Form1.Line1.X2 = X
            Form1.Line1.Y2 = Y
            If dPoint = 1 Then
                Call dPoint_Size(X, Y, Format$(Sqr((MouseX - X) * (MouseX - X) + (MouseY - Y) * (MouseY - Y)), "####0.00"))
            End If
        Case 2
            Call Drawing_Spray(X, Y)
        Case 3
            sX = SmallL(MouseX, X)
            tX = GapL(MouseX, X)
            sY = SmallL(MouseY, Y)
            tY = GapL(MouseY, Y)
            If dPoint = 1 Then
                Call dPoint_Size(X, Y, Format$(tX, "###0") + "x" + Format$(tY, "###0"))
            End If
            Form1.Shape1.Move sX, sY, tX, tY
            Form1.Shape4.Move sX, sY, tX, tY
        Case 4
            sX = SmallL(MouseX, X)
            tX = GapL(MouseX, X)
            sY = SmallL(MouseY, Y)
            tY = GapL(MouseY, Y)
            If dPoint = 1 Then
                Call dPoint_Size(X, Y, Format$(tX, "###0") + "x" + Format$(tY, "###0"))
            End If
            Form1.Shape2.Move sX, sY, tX, tY
            Form1.Shape4.Move sX, sY, tX, tY
        Case 6
            Form1.Label1.Move X, Y
        Case 7
            Dim hX As Single
            Dim hY As Single
            Dim w As Single
            Dim h As Single
            w = Form1.Shape3.Width
            h = Form1.Shape3.Height
            hX = X - w * 0.5
            hY = Y - h * 0.5
            BitBlt Form1.hDC, CLng(hX), CLng(hY), CLng(w), CLng(h), Form2.Picture2.hDC, CLng(hX), CLng(hY), SRCCOPY
            Form2.Picture4.Line (hX, hY)-(hX + w, hY + h), vbWhite, BF
            Form2.Picture5.Line (hX, hY)-(hX + w, hY + h), vbBlack, BF
            Form1.Shape3.Move hX, hY
        Case Else
        End Select
    End If
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If MouseFlag = 0 Then
        If Button = vbRightButton Then
            PopupMenu Form2.PMenu
            Exit Sub
        End If
    ElseIf MouseFlag = 1 Then
        If Button = vbRightButton Then Exit Sub
        MouseFlag = 0
        Select Case dMode
        Case 1
            Form1.DrawWidth = dWidth
            Form2.Picture4.DrawWidth = dWidth
            Form2.Picture5.DrawWidth = dWidth
            Form1.Line (MouseX, MouseY)-(X, Y), dColor
            Form2.Picture4.Line (MouseX, MouseY)-(X, Y), vbBlack
            Form2.Picture5.Line (MouseX, MouseY)-(X, Y), dColor
            Form1.Line1.Visible = False
            If dPoint = 1 Then
                Form1.Line2.Visible = False
                Form1.Label3.Visible = False
                Form1.Label4.Visible = False
            End If
        Case 3
            Dim cX As Single
            Dim cY As Single
            Dim sX As Single
            Dim sY As Single
            Dim tX As Single
            Dim tY As Single
            Dim r As Single
            Dim rdi As Single
            sX = SmallL(MouseX, X)
            tX = GapL(MouseX, X)
            sY = SmallL(MouseY, Y)
            tY = GapL(MouseY, Y)
            cX = sX + tX / 2
            cY = sY + tY / 2
            r = (-tX * (tX > tY) - tY * (tX <= tY)) / 2
            rdi = tY / tX
            Form1.DrawWidth = dWidth
            Form2.Picture4.DrawWidth = dWidth
            Form2.Picture5.DrawWidth = dWidth
            Form1.FillColor = dColor
            Form2.Picture4.FillColor = vbBlack
            Form2.Picture5.FillColor = dColor
            Form1.Circle (cX, cY), r, dColor, , , rdi
            Form2.Picture4.Circle (cX, cY), r, vbBlack, , , rdi
            Form2.Picture5.Circle (cX, cY), r, dColor, , , rdi
            Form1.Shape1.Visible = False
            Form1.Shape4.Visible = False
            If dPoint = 1 Then
                Form1.Line2.Visible = False
                Form1.Label3.Visible = False
                Form1.Label4.Visible = False
            End If
        Case 4
            Form1.DrawWidth = dWidth
            Form2.Picture4.DrawWidth = dWidth
            Form2.Picture5.DrawWidth = dWidth
            Form1.FillColor = dColor
            Form2.Picture4.FillColor = vbBlack
            Form2.Picture5.FillColor = dColor
            Form1.Line (MouseX, MouseY)-(X, Y), dColor, B
            Form2.Picture4.Line (MouseX, MouseY)-(X, Y), vbBlack, B
            Form2.Picture5.Line (MouseX, MouseY)-(X, Y), dColor, B
            Form1.Shape2.Visible = False
            Form1.Shape4.Visible = False
            If dPoint = 1 Then
                Form1.Line2.Visible = False
                Form1.Label3.Visible = False
                Form1.Label4.Visible = False
            End If
        Case 5
            Dim rd As Integer
            Dim gr As Integer
            Dim bl As Integer
            Dim c As Long
            Dim i As Integer
            dColor = Form1.Point(X, Y)
            c = dColor
            bl = CInt(c \ &H10000)
            c = c Mod &H10000
            gr = CInt(c \ &H100)
            c = c Mod &H100
            rd = CInt(c)
            Form3.HScroll1.Value = rd
            Form3.HScroll2.Value = gr
            Form3.HScroll3.Value = bl
            dMode = dModeBefore
            For i = 0 To 5
                Form2.PM_dMode(i).Checked = False
            Next i
            For i = 0 To 6
                Form3.Command2(i).BackColor = QBColor(7)
            Next i
            Form3.Command2(dMode).BackColor = vbYellow
            Form2.PM_dMode(dMode).Checked = True
            dModeStg2 = Form3.Command2(dMode).Caption
            Call F2Caption
        Case 6
            Dim l As Long
            Dim pX As Long
            Dim pY As Long
            Form1.Label1.Visible = False
            Form1.ForeColor = dColor
            Form1.FontSize = 10 + dWidth * 3
            Form2.Picture4.ForeColor = vbBlack
            Form2.Picture5.ForeColor = dColor
            Form2.Picture4.FontSize = 10 + dWidth * 3
            Form2.Picture5.FontSize = 10 + dWidth * 3
            l = CLng(LenX(dText))
            SetTextAlign Form1.hDC, TA_DEFAULT
            SetTextColor Form1.hDC, dColor
            SetTextAlign Form2.Picture4.hDC, TA_DEFAULT
            SetTextAlign Form2.Picture5.hDC, TA_DEFAULT
            SetTextColor Form2.Picture4.hDC, vbBlack
            SetTextColor Form2.Picture5.hDC, dColor
            pX = CLng(X)
            pY = CLng(Y)
            TextOut Form1.hDC, pX, pY, dText, l
            TextOut Form2.Picture4.hDC, pX, pY, dText, l
            TextOut Form2.Picture5.hDC, pX, pY, dText, l
            Form1.Refresh
        Case 7
            Form1.Shape3.Visible = False
        Case Else
        End Select
    End If

End Sub

